\input{preamble}

\begin{document}
	\includecourse

	\textbf{Notations}

	Soit $n \in \N$, l'ensemble des polynômes de degré inférieur ou égal à $n$ se note
	$\polyrn{n}$

	\begin{demo}
		\textbf{Proposition 2 page 3}

		\begin{itemize}
			\item Soient $P, Q \in \polyr$ tels que $PQ = \nulp$

			On en déduit que : $\degp ( PQ ) = -\infty$,

			c'est-à-dire : $\degp ( P ) + \degp( Q ) = -\infty$

			On en déduit que $\degp( P ) = -\infty \ou \degp( Q ) = -\infty$,

			\Finalement $P = \nulp \ou Q = \nulp$
		\end{itemize}
	\end{demo}

	\textbf{Dérivation}

	\begin{expl}
		Soit $P = X^3 - 2X^2 + 3X + 6$

		\begin{align*}
			P' &= 3X^2 - 4X + 3 \\
			P'' &= 6X - 4 \\
			P^{( 3 )} &= 6 \\
			P^{( 4 )} &= \nulp \\
			\forall k \ge 4, P^{( k )} &= \nulp
		\end{align*}
	\end{expl}

	\begin{demo}
		\textbf{Proposition 4 page 5 : point 3} 

		\highlighty{$\impliedby$}

		Supposons qu'il existe $\lambda \in \mathbb{K}^*$ tel que : $A = \lambda B$

		Fixons un tel  $\lambda$

		D'une part $\lambda \in \polyr$, donc : $B  \mid A$.

		D'autre part $B = \frac{1}{\lambda}A$ et $\frac{1}{\lambda} \in \polyr$,

		Donc $A  \mid B$.

		\Finalement : $A \mid B \et B \mid A$ 

		\highlighty{$\implies$}

		Supposons que : $A  \mid B \et B \mid A$.

		\begin{itemize}
			\item Cas $A = \nulp$. On a alors $B = \nulp$, donc $A = 1 \cdot B$.

			En posant $\lambda = 1$, on a $\lambda \in \mathbb{K}^*$ et $A = \lambda B$.

			\item Cas $A \neq 0$ 

			Il existe $P, Q \in \polyr$ tels que :
			
			$B = PA$ et $A = QB$.

			On en déduit que $A = QPA$, 

			donc $( 1_{\polyr} - QP )A = \nulp$.

			L'anneau $\polyr$ étant intègre, on en déduit que $QP = 1_{\polyr}$.

			Donc $\degp P + \degp Q = 0$, or $\degp P \ge 0 \et \degp Q \ge 0$,

			donc $\degp P = \degp Q = 0$.

			C'est-à-dire que $P$ et $Q$ sont des polynômes.

			On pose $\lambda = Q$.

			On a : $A = \lambda B$ et $\lambda \in \mathbb{K}^*$.
		\end{itemize}
	\end{demo}

	\begin{demo}
		\textbf{Proposition 7 page 9} 

		Effectuons la division euclidienne de $P$ par $X - \lambda$ 

		$P = ( X - \lambda )Q + R$ avec $Q, R \in \polyr$ et $\degp R < 1 \; ( 1 )$ 

		On en déduit que $R$ est un polynôme constant, notons $\mu$ cette constante

		En évaluant l'égalité (1) en $\lambda$, on obtient :

		$P( \lambda ) = ( \lambda - \lambda )Q( \lambda ) + \mu = \mu$ 

		\Finalement l'égalité (1) devient : $P = ( X - \lambda )Q + P( \lambda )$
	\end{demo}

	\begin{demo}
		\textbf{Proposition 8 page 9} 

		Soient $P \in \polyr$ et $\lambda \in \mathbb{K}$ 

		\highlighty{Supposons que $\lambda$ soit racine de $P$}

		D'après la proposition 7, en effectuant la division euclidienne de $P$ par $X -
		\lambda$, on obtient : 
		\begin{align*}
			P = ( X - \lambda )Q + P( \lambda ) \text{ où } Q \in \polyr
		\end{align*}

		Or $P( \lambda ) = 0$,

		donc $P = ( X - \lambda )Q$ 

		Donc $X - \lambda$ divise $P$.

		\highlighty{Supposons que $X - \lambda$ divise $P$}

		On en déduit qu'il existe $Q \in \polyr$ tel que : $P = ( X - \lambda )Q$. 

		En évaluant cette égalité en $\lambda$ on obtient :
		\begin{align*}
			P( \lambda ) = ( \lambda - \lambda)Q( \lambda ) = 0
		\end{align*}

		Donc \highlighty{$\lambda$ est racine de $P$}
	\end{demo}

	\begin{exo}
		\textbf{1 page 9} 
		
		Soit $n \in \N$ 

		Déterminer le reste de la division euclidienne de $X^n$ par $X^2 + X - 2$.

		Notons respectivement $Q$ et $R$ le quotient et le reste de la division
		euclidienne de $X^n$ par $X^2 + X - 2$.

		On a : $X^n = ( X^2 + X - 2 )Q + R$, avec $\degp R < 2$.

		$R$ s'écrit donc $R = aX + b$ où $a, b \in \R$ 

		On a donc :
		\begin{align*}
			X^n = ( X^2 + X - 2 )Q + aX + b \;( * )
		\end{align*}

		{\color{violet}{Idée : évaluer l'égalité $(*)$ en les racines du polynôme $X^2 + X -
		2$}}

		En évaluant $( * )$ en 1 on en déduit que : $1^n = 0\times Q( 1 ) + a + b$ i.e. $1
		= a + b$ 

		En évaluant $( * )$ en $-2$ on en déduit que :
		
		$( -2 )^n = 0\times Q( -2 ) - 2a + b$ i.e. $( -2 )^n = -2a + b$ 

		On résout donc :
		\begin{align*}
			\left\{
			\begin{NiceMatrix}
				a &+& b &= 1 \\
				-2a &+& b &= ( -2 )^n 
			.\end{NiceMatrix}
			\right. 
			\iff&
			\left\{
			\begin{NiceMatrix}
				a &+& b &= 1 \\
				-3a && &= ( -2 )^n  -1
			.\end{NiceMatrix}
			\right. \\
			\iff& 
			\left\{
			\begin{NiceMatrix}
				b &= \frac{1}{3}( 2 + ( -2 )^n ) \\
				a &= \frac{1}{3}( -( -2 )^n + 1 ) 
			.\end{NiceMatrix}
			\right.
		\end{align*} 

		\Conclusion Le reste de la division euclidienne de $X^n$ par $X^2 + X - 2$ est :
		\[
		R = \frac{1}{3}( -( -2 )^n + 1 )X + \frac{1}{3}( 2 + ( -2 )^n )
		\]
	\end{exo}

	\begin{exo}
		\textbf{2 page 9} 

		\bigskip
		$z^4 + z^3 + 3z^2 + 2 = 0 \; ( E )$

		\subsection{Montrer que $( E )$ admet deux solutions imaginaires pures opposées}%

		\begin{itemize}
			\item Soit $x \in \R$ 

			\begin{align*}
				ix \text{ est sol de } ( E ) \iff& ( ix )^4 + ( ix )^3 + 3( ix )^2 + 2( ix
				) + 2 = 0 \\
				\iff& x^4 - ix^3 - 3x^2 + 2ix + 2 = 0 \\
				\iff& ( x^4 - 3x^2 + 2 ) + i( -x^3 + 2x ) = 0 \\
				\iff& 
				\left\{
				\begin{NiceMatrix}
					( x^2 )^2 - 3x^2 + 2 = 0 \\
					-x^3 + 2x = 0
				.\end{NiceMatrix}
				\right. \\
				\iff&
				\left\{
				\begin{NiceMatrix}
					( x^2 - 2 )( x^2 - 1 ) = 0 \\
					-x( x^2 - 2 ) = 0
				.\end{NiceMatrix}
				\right. \\
				\iff& 
				\left\{
				\begin{NiceMatrix}
					( x^2 - 2 ) = 0 \ou ( x^2 - 1 ) = 0 \\
					x = 0 \ou ( x^2 - 2 ) = 0
				.\end{NiceMatrix}
				\right. \\
				\iff& x^2 - 2 = 0 \\
				\iff& x = \sqrt{2} \ou x = -\sqrt{2}
			\end{align*}
		\end{itemize}

		$( E )$ admet donc deux solutions imaginaires pures qui sont $\sqrt{2}i$ et
		$-\sqrt{2}i$.

		Considérons le polynôme, de $\mathbb{C}[X], P = X^4 + X^3 + 3X^2 + 2X + 2$.

		$i\sqrt{2}i$ et $-\sqrt{2}i$ sont racines de $P$ donc $( X - \sqrt{2}i  )( X +
		\sqrt{2}i ) = X^2 + 2$ divise $P$.

		\polyeucdiv{X^4 + X^3 + 3X^2 + 2X + 2}{X^2 + 2}

		\Finalement :  $X^4 + X^3 + 3x^2 + 2X + 2 = ( X^2  +2 )( X^2 + X + 1 )$ 

		On déduit de cette égalité polynômiale que :

		$\forall z \in \C, z^4 + z^3 + 3z^2 + 2z + 2 = ( z^2 + 2 )( z^2 + z + 1  )$.

		Retour à la résolution de $( E )$ :
		\begin{align*}
			( E ) \iff& ( z^2 + 2 )( z^2 + 2 + 1 ) = 0 \\
			\iff& x^2 + 2 = 0 \ou z^2 + z + 1 = 0 \\
			\iff& z = -\sqrt{2}i \ou z = \sqrt{2}i \ou z = j \ou z = j^2
		\end{align*}

		\Conclusion : L'ensemble des solutions de $( E ) $ est :
		\begin{align*}
			\mathscr{S} = \set{ -\sqrt{2}i ; \sqrt{2}i ; j ; j^2 } 
		\end{align*}
	\end{exo}

	\begin{demo}
		\textbf{Définition 11 page 9} 

		$P \in \polyr$  non nul et $\lambda \in \mathbb{K}$ 

		$\mathscr{E} = \set{ k \in \N, ( X-\lambda )^k \text{ divise } P } $ 

		\begin{itemize}
			\item $\mathscr{E}$ est une partie de $\N$.

			\item $\mathscr{E}$ est non vide en effet : $( X -\lambda )^0=1$ divise $P$ donc
			$0 \in \mathscr{E}$ 

			\item $\mathscr{E}$ est majorée (par $\degp P$) en effet :

			\begin{itemize}
				\item Soit $k \in \mathscr{E}$.

				$P$ s'écrit donc $P = ( X-\lambda )^k Q$ où $Q \in \polyr$ 

				d'où $\degp P = k+\degp Q$.

				Or $P\neq 0$ donc $Q\neq 0$,

				on en déduit que $\degp Q \ge 0$,

				finalement $\degp P \ge k$.
			\end{itemize}

			$\mathscr{E}$ est une partie non vide et majorée de $\N$ donc $\mathscr{E}$ admet un
			plus grand élément.

			Notons $m$ le plus grand élément de $\mathscr{E}$.

			On a immédiatement que :

			\begin{itemize}
				\item $P$ est divisible par $( X-\lambda )^m$ mais pas par $( X-\lambda
				)^{m+1}$.

				\item Il existe $Q \in \polyr$ tel que $P = ( X-\lambda )^mQ$ et $Q(
				\lambda ) \neq 0$
			\end{itemize}
		\end{itemize}
	\end{demo}

	\textbf{Exemple 1 page 10} 

	Soit $P=( X-1 )^3( X-4 )^2( X+5 )$ 
	
	Donner les racines de $P$ ainsi que leur ordre de multiplicité.

	\begin{itemize}
		\item $1$ est racine de multiplicité $3$ 

		\item $4$ est racine double

		\item $-5$ est racine simple
	\end{itemize}

	On dit que $P$ a $6$ racines comptées avec multiplicité.

	\begin{demo}
		\textbf{Proposition 11 page 10} 

		Supposons que $\lambda$ est racine de multiplicité $m$ de $P$.

		On en déduit que $P$ s'écrit $P = ( X-\lambda )^mQ$ où $Q \in \polyr$ et $Q(
		\lambda )\neq 0$.
		\begin{align*}
			P &= \sum_{i=0}^{+\infty} \frac{P^{( i )}( \lambda )}{i!}( X-\lambda )^i \\
			&= \sum_{i=m}^{+\infty} \frac{P^{( i )}( \lambda )}{i!}( X-\lambda )^i +
			\sum_{i=0}^{m-1} \frac{P^{( i )}( \lambda )}{i!}( X-\lambda )^i \\
			&= ( X-\lambda )^m \underbrace{ \sum_{i=m}^{+\infty} \frac{p^{( i )}( \lambda )}{i!}(
			x-\lambda )^{i-m} }_{\in \polyr} +\underbrace{ \sum_{i=0}^{m-1} \frac{P^{( i
			)}( \lambda )}{i!}( X-\lambda )^i  }_{=R}\\
		\end{align*}

		On a $\degp R < m$ 

		Par unicité de la division euclidienne du polynôme $P$ par le polynôme $(
		X-\lambda )^m$ on en déduit que :

		$\R = \nulp \;( * )$ et $Q= \sum_{i=m}^{+\infty} \frac{P^{( i )}( \lambda )}{i!}(
		X-\lambda )^{i-m} \;( ** )$

		\begin{itemize}
			\item On a $R = \sum_{i=0}^{m-1} \frac{P^{(i)}( \lambda )}{i!}( X-\lambda )^i $

			donc $R( X+\lambda )=\sum_{i=0}^{m-1} \frac{P^{( i )}( \lambda )}{i!}X^i$

			On déduit de $( * )$ que $R( X+\lambda ) = \nulp$,

			donc \highlighty{$\forall i \in \intervalleEntier{0}{m-1}, P^{( i )}( \lambda
			)=0$}

			\item En évaluant $( **  )$ en $\lambda$ on en déduit que $Q( \lambda ) =
			\frac{P^{( m )}( \lambda )}{m!}$, or $Q( \lambda )\neq 0$,

			donc : \highlighty {$P^{( m )}( \lambda )\neq 0$}
		\end{itemize}

		Réciproquement, supposons que : $\forall i \in \intervalleEntier{0}{m-1}, P^{( i
		)}( \lambda )=0$ et $P^{( m )}( \lambda )\neq 0$ 

		D'après la formule de Taylor :
		\begin{align*}
			P &= \sum_{i=0}^{+\infty} \frac{P^{( i )}( \lambda )}{i!}( X-\lambda )^i \\
			&= \sum_{i=m}^{+\infty} \frac{P^{( i )}( \lambda )}{i!}( X-\lambda )^i +
			\underbrace{\sum_{i=0}^{m-1} \frac{P^{( i )}( \lambda )}{i!}( X-\lambda )^i
			}_{=0}\\
			&= \sum_{i=m}^{+\infty} \frac{P^{( i )}( \lambda )}{i!}( X-\lambda )^i \\
			&= ( X-\lambda )^m \underbrace{ \sum_{i=m}^{+\infty} \frac{p^{( i )}( \lambda )}{i!}(
			x-\lambda )^{i-m} }_{=Q\in \polyr} \\
		\end{align*}

		On a donc $P=( X-\lambda )^m Q$ où $Q \in \polyr$.

		De plus :
		\[
		Q( \lambda )=\frac{P^{( m )}( \lambda )}{m!}\neq 0
		.\] 

		\Finalement : $\lambda$ est racine de multiplicité $m$ de $P$
	\end{demo}

	\begin{demo}
		\textbf{Conséquence 3 de la proposition 12 page 10} 

		Soit $P \neq \nulp$

		On suppose que $\lambda_1, \ldots, \lambda_r$ sont des racines distinctes $2$ à
		$2$ de multiplicités respectives $m_1, \ldots, m_r$ 

		On suppose que $\degp( P ) = m_1+m_2+\ldots+m_r$ 

		D'après la proposition 12, $P$ s'écrit $P = ( X-\lambda_1 )^{m_1}\times (
		X-\lambda_2 )^{m_2}\times \ldots\times ( x-\lambda_r )^{m_r} \times Q $ où $Q \in
		\polyr$

		Donc $\degp( P ) = m_1+m_2+\ldots+m_r+\degp( Q )$,

		or $\degp( P )=m_1+m_2+\ldots+m_r$,

		donc $\degp( Q )=0$ 

		Finalement $Q$ est un polynôme constant non nul.
	\end{demo}

	\begin{exo}
		Montrer que la fonction sin n'est pas une fonction polynomiale.

		Raisonnons par l'absurde et supposons qu'il existe un polynôme $P$ tel que
		$\tilde{P} = \sin$,

		c'est-à-dire tel que : $\forall x \in \R, P( x )=\sin(x)$ 

		En particulier : $\forall k \in \Z, P( k\pi )=0$ 

		$P$ admet donc une infinité de racines, d'où $P = \nulp$ 

		On en déduit alors que : $\forall x \in \R, \sin(x)= 0$. \textsc{Contradiction}

		Finalement la fonction sin n'est pas polynomiale.
	\end{exo}


	\begin{exo}
		Montrer que la fonction suivante n'est pas une fonction polynomiale :
		\begin{align*}
			f: \R &\longrightarrow \R \\
			x &\longmapsto \frac{x^4 +1}{x^2+1} 
		.\end{align*}

		Raisonnons par l'absurde et supposons que $f$ est une fonction polynomiale,
		c'est-à-dire qu'il existe $P \in \mathbb{R}[X]$ tel que $f = \tilde{P}$ 

		On fixe $P$.

		On en déduit que : $\forall x \in \R, \frac{x^4+1}{x^2+1}=P( x )$ 

		d'où : $\forall x \in \R, x^4 + 1 = ( x^2+1 )P( x )$ 

		Donc : $X^4 + 1 = ( X^2+1 )P$ 

		En évaluant en $i$ on obtient :
		$i^4 + 1 =0$ 

		Ce qui est faux.

		Finalement $f$ n'est pas une fonction polynomiale.
	\end{exo}

	\begin{demo}
		\textbf{Proposition 13 page 11} 

		Montrons que : $\forall P \in \mathbb{R}[X], \lambda \in \C, P( \lambda ) = 0 \iff
		P ( \overline{\lambda} )= 0 \;( * )$ 

		Soient $P = \sum_{k=0}^{n} a_k X^k \in \mathbb{R}[X], \lambda \in \C $ 

		\begin{align*}
			P( \lambda ) = 0 \iff& = \sum_{k=0}^{n} a_k \lambda^k = 0 \\
			\iff& \overline{\sum_{k=0}^{n} a_k \lambda^k} = 0 \\
			\iff& \sum_{k=0}^{n} \overline{a_k}( \overline{\lambda} )^k = 0\\
			\iff& \sum_{k=0}^{n} a_k( \overline{\lambda} )^k = 0 \text{ car } a_0, \ldots,
			a_n \in \R \\
			\iff& P( \overline{\lambda} ) = 0
		\end{align*}

		On suppose que $\lambda$ est racine de $P$ de multiplicité $m$.

		On déduit de la proposition 11 que :
		\[
		\forall i \in \intervalleEntier{0}{m-1}, P^{( i )}( \lambda ) = 0 \et P^{( m )}(
		\lambda )\neq 0
		.\] 

		Or, pour $i \in \intervalleEntier{0}{m}, P^{( i )} \in \mathbb{R}[X]$,

		donc d'après $( * )$ :
		\[
		\forall i \in \intervalleEntier{0}{m-1}, P^{( i )}( \overline{\lambda} )=0 \et
		P^{( m )}( \overline{\lambda} ) \neq 0
		\]
		D'où \highlighty{$\overline{\lambda}$ est racine de $P$ de multiplicité $m$}
	\end{demo}

	\textbf{Exercice page 11} 

	\begin{align*}
		\left\{
		\begin{NiceMatrix}
			x+y=3 \\
			x^2+y^2=5
		.\end{NiceMatrix}
		\right.
		\iff& 
		\left\{
		\begin{NiceMatrix}
			x+y=3\\
			( x+y )^2-2xy=5
		.\end{NiceMatrix}
		\right.\\
		\iff&
		\left\{
		\begin{NiceMatrix}
			x+y=3\\
			-2xy=5-3^2
		.\end{NiceMatrix}
		\right.\\
		\iff&
		\left\{
		\begin{NiceMatrix}
			x+y=3\\
			xy=2
		.\end{NiceMatrix}
		\right.
	\end{align*}

	$x$ et $y$ sont donc les racines du polynôme $X^2-3X+2$.

	Or $X^2-3X+2=( X-1 )( X-2 )$,

	les racines de $X^2-3X+2$ sont donc 1 et 2.

	\Finalement l'ensemble des solutions du système est :
	\[
		\mathscr{S}=\set{ ( 1,2 );( 2,1 ) } 
	.\] 

	\begin{demo}
		\textbf{Proposition 14 page 12} 

		\begin{itemize}
			\item \highlighty{On suppose que $P$ est irréductible.}

			$P$ est donc de degré au moins 1, 

			en appliquant le théorème de d'Alembert-Gauss on en déduit que $P$ admet au moins
			une racine  $\lambda \in \C$.

			Donc $X-\lambda$ divise $P$ dans $\mathbb{C}[X]$ 

			$P$ étant irréductible dans  $\C$, $P$ et $X-\lambda$ sont associés.

			\highlighty{Donc $P$ est de degré 1.}

			\item \highlighty{On suppose que $P$ est un polynôme de degré 1}

			Soit $Q \in \mathbb{C}[X]$ un diviseur de $P$.

			$P$ s'écrit $P=QR$ où $R\in \mathbb{C}[X]$.

			On en déduit que $1 = \degp Q + \degp R$ 

			donc $( \degp Q=0 \et \degp R = 1 )\ou( \degp Q=1 \et \degp R=0 )$ 

			\begin{itemize}
				\item Cas $\degp Q= 0$, $Q$ est donc une constante non nulle. 
				\item Cas $\degp Q = 1$ 

				On a $P = \alpha Q$ où $\alpha \in \C^*$,

				d'où $P$ et $Q$ sont associés.
			\end{itemize}

			\Finalement $P$ est irréductible dans $\C$.
		\end{itemize}

		\highlighty{On suppose que $P$ est irréductible}

		$P$ est donc de degré au moins 1,

		en appliquant le théorème de d'Alembert-Gauss on en déduit que $P$ admet au moins
		une racine $\lambda \in \C$.

		Donc $X-\lambda$ divise $P$ dans $\mathbb{C}[X]$.

		\begin{itemize}
			\item 1er cas : $\lambda \in \R$

			$X-\lambda$ divise $P$ dans $\mathbb{R}[X]$,

			de plus $P$ est irréductible sur  $\R$,

			donc $P$ et $X-\lambda$ sont associés, 

			donc $P$ est de degré 1.

			\item 2ème cas : $\lambda \not\in \R$

			$\lambda$ et $\overline{\lambda}$ sont deux racines distinctes de $P$, 

			donc $Q=( X-\lambda )( X-\overline{\lambda} )$ divise $P$ dans
			$\mathbb{C}[X]$,

			Or $Q=X^2-( \lambda+\overline{\lambda} )X+\lambda \overline{\lambda} \in
			\mathbb{R}[X]$,

			d'où $Q$ divise $P$ dans $\mathbb{R}[X]$ 

			$P$ étant irréductible dans $\R$, 

			on en déduit que $P$ et $Q$ sont associés, 

			donc $P$ est de degré 2. 

			\Finalement $P$ est un polynôme de degré 1 ou de degré 2 et de discriminant
			strictement négatif.
		\end{itemize}
	\end{demo}

	\textbf{Exercice 1 page 13} 

	Décomposons $P$ dans $\C : P = ( X-i )^2( X+i )^2$.

	Les racines de $P$ sont $i$ et $-i$ de multiplicité 2.

	\highlighty{Montrons que $i$ est racine multiple de $Q$}:

	\begin{align*}
		Q(i) &=5 i^{13}+10 i^{11}+5 i^{9}-3 i^{5}+i^{4}-6 i^{3}+2 i^{2}-3 i+1 \\
		&=5 i-10 i+5 i-3 i+1+6 i-2-3 i+1 \\
		&=0 \\
	\end{align*}
	$Q'= 65 X^{12}+110 X^{10}+45 X^{8}-15 X^{4}+4 X^{3}-18 X^{2}+4 X-3$, donc :
	\begin{align*}
		&Q'(i)=65 i^{12}+110 i^{10}+45 i^{8}-15 i^{4}+4 i^{3}-18 i^{2}+4 i-3\\
		&=65-110+45-15-4 i+18+4 i-3 \\
		&=0
	\end{align*}

	Finalement $Q(i)=Q'(i)=0$
	donc $i$ est racine de multiplicité au moins 2 de $Q$. $Q$ est à coefficients dans
	$\R$ donc $-i$ est racine de multiplicité au moins 2 de $Q$.
	
	\Conclusion $P=( X-i )^2( X+i )^2$ et $i$ et $-i$ sont racines de multiplicité au
	moins 2 de $Q$,

	donc $P$ divise $Q$.

	\begin{demo}
		\textbf{Proposition 17 page 13} 

		Soit $P \in \mathbb{R}[X]$

		On peut le considérer à coefficients complexes.

		On nomme les racines de $P$ dans $\C$
		$\underbrace{\lambda_1,\ldots,\lambda_r}_{\in
		\R},\underbrace{\alpha_1,\ldots,\alpha_k}_{\not\in \R}$ de multiplicité
		$m_1,\ldots,m_r, n_1,\ldots,n_k$

		$P$ s'écrit $P=\underbrace{\alpha}_{\neq O} \prod_{i=1}^{r} \underbrace{(
		X-\lambda_i )}_{\in \mathbb{R}[X]}^{m_i}
		\prod_{j=1}^{k} \underbrace{( X-\lambda_j )}_{\not\in \mathbb{R}[X]}^{n_j} $

		Soit $j \in \intervalleEntier{1}{k}$ 

		$\alpha_j$ racine de $P$ de multiplicité $n_j$, comme $P \in \mathbb{R}[X]$

		$\overline{\alpha_j}$ est aussi racine de $P$ de multiplicité $n_j ( X-\lambda_j
		)^{n_j}( X-\overline{\lambda_j}^{n_j} )=\left( X^2-( \lambda_j + \overline{\lambda_j} )
		X + \lambda_j \overline{\lambda_j}\right)^{n_j}$
	\end{demo}

	
\end{document}
